import * as config from './environments/environment';
import * as prodConfig from './environments/environment.prod';

describe('development configuration', () => {
  it('provides the right values', () => {
    expect(config.environment.production).toBe(false);
    expect(config.environment.apiUrl).toBe('https://gabriels-todos-app.firebaseio.com');
    expect(config.environment.apiKey).toBe('AIzaSyBwBCgUmhQqf8Zc1nxgMxf3IvlVGEsRkLE');
    expect(config.environment.authDomain).toBe('gabriels-todos-app.firebaseapp.com');
    expect(config.environment.authTokenParamName).toBe('auth');
    expect(config.environment.uidPlaceholder).toBe('$UID$');
    expect(config.environment.uidPlaceholderRegex).toEqual(/\$UID\$/);
  });
});

describe('production configuration', () => {
  it('provides the right values', () => {
    expect(prodConfig.environment.production).toBe(true);
    expect(prodConfig.environment.apiUrl).toBe('https://gabriels-todos-app.firebaseio.com');
    expect(prodConfig.environment.apiKey).toBe('AIzaSyBwBCgUmhQqf8Zc1nxgMxf3IvlVGEsRkLE');
    expect(prodConfig.environment.authDomain).toBe('gabriels-todos-app.firebaseapp.com');
    expect(prodConfig.environment.authTokenParamName).toBe('auth');
    expect(prodConfig.environment.uidPlaceholder).toBe('$UID$');
    expect(prodConfig.environment.uidPlaceholderRegex).toEqual(/\$UID\$/);
  });
});
