export const environment = {
  production: true,
  apiUrl: 'https://gabriels-todos-app.firebaseio.com',
  apiKey: 'AIzaSyBwBCgUmhQqf8Zc1nxgMxf3IvlVGEsRkLE',
  authDomain: 'gabriels-todos-app.firebaseapp.com',
  authTokenParamName: 'auth',
  uidPlaceholder: '$UID$',
  uidPlaceholderRegex: /\$UID\$/
};
