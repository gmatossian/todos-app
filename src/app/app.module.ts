import { AuthGuard } from './auth/auth-guard.service';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AuthService } from './auth/auth.service';
import { TodosService } from './todos/todos.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TodosComponent } from './todos/todos.component';
import { CreateUpdateComponent } from './todos/create-update/create-update.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { ListOfTodosComponent } from './todos/list-of-todos/list-of-todos.component';
import { VerifyEmailComponent } from './auth/verify-email/verify-email.component';
import { UnauthenticatedComponent } from './auth/unauthenticated/unauthenticated.component';

const appRoutes: Routes = [
  { path: '', component: TodosComponent, canActivate: [AuthGuard] },
  { path: 'create', component: CreateUpdateComponent, canActivate: [AuthGuard] },
  { path: ':id/edit', component: CreateUpdateComponent, canActivate: [AuthGuard] },
  { path: 'signup', component: SignupComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'unauthenticated', component: UnauthenticatedComponent },
  { path: 'unverified', component: VerifyEmailComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TodosComponent,
    CreateUpdateComponent,
    SignupComponent,
    SigninComponent,
    ListOfTodosComponent,
    VerifyEmailComponent,
    UnauthenticatedComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,  // TODO: Remove when no longer needed
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    TodosService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true
    },
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
