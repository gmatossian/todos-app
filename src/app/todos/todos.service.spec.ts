import { Todo } from './todo.model';
import { TodosService } from './todos.service';
import { TestBed, inject, async } from '@angular/core/testing';
import { HttpClientModule, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('TodosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        TodosService
      ]
    });
  });

  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify();
  }));

  describe('deleteTodo', () => {
    const requestMatcher = (req: HttpRequest<any>) => {
      return req.url === 'https://gabriels-todos-app.firebaseio.com/todos/$UID$/foo.json'
        && req.method === 'DELETE';
    };
    let failed: boolean;

    beforeEach(() => {
      failed = false;
    });

    it('calls the backend', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.deleteTodo(new Todo('foo', 'bar', 'baz')).subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher);

        expect(failed).toBe(false);
    })));

    it('fails if backend call fails', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.deleteTodo(new Todo('foo', 'bar', 'baz')).subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush(null, { status: 500, statusText: 'Error' });

        expect(failed).toBe(true);
    })));
  });

  describe('updateTodo', () => {
    const requestMatcher = (req: HttpRequest<any>) => {
      return req.url === 'https://gabriels-todos-app.firebaseio.com/todos/$UID$/foo.json'
        && req.method === 'PATCH'
        && req.body.title === 'bar'
        && req.body.description === 'baz';
    };
    let failed: boolean;

    beforeEach(() => {
      failed = false;
    });

    it('calls the backend', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.updateTodo('foo', 'bar', 'baz').subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher);

        expect(failed).toBe(false);
    })));

    it('fails if backend call fails', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.updateTodo('foo', 'bar', 'baz').subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush(null, { status: 500, statusText: 'Error' });

        expect(failed).toBe(true);
    })));
  });

  describe('addTodo', () => {
    const requestMatcher = (req: HttpRequest<any>) => {
      return req.url === 'https://gabriels-todos-app.firebaseio.com/todos/$UID$.json'
        && req.method === 'POST'
        && req.body.title === 'foo'
        && req.body.description === 'bar';
    };
    let failed: boolean;

    beforeEach(() => {
      failed = false;
    });

    it('calls the backend', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.addTodo('foo', 'bar').subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher);

        expect(failed).toBe(false);
    })));

    it('fails if backend call fails', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.addTodo('foo', 'bar').subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush(null, { status: 500, statusText: 'Error' });

        expect(failed).toBe(true);
    })));
  });

  describe('getTodoById', () => {
    const requestMatcher = (req: HttpRequest<any>) => {
      return req.url === 'https://gabriels-todos-app.firebaseio.com/todos/$UID$/foo.json'
        && req.method === 'GET';
    };
    let failed: boolean;

    beforeEach(() => {
      failed = false;
    });

    it('calls the backend; constructs and returns Todo', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        let response: Todo;
        service.getTodoById('foo').subscribe(next => response = next, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush({ title: 'bar', description: 'baz' });

        expect(response).toEqual(new Todo('foo', 'bar', 'baz'));
        expect(failed).toBe(false);
    })));

    it('fails if backend call fails', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        let response: Todo;
        service.getTodoById('foo').subscribe(next => response = next, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush(null, { status: 500, statusText: 'Error' });

        expect(failed).toBe(true);
    })));
  });

  describe('getTodos', () => {
    const requestMatcher = (req: HttpRequest<any>) => {
      return req.url === 'https://gabriels-todos-app.firebaseio.com/todos/$UID$.json'
        && req.method === 'GET';
    };
    let failed: boolean;

    beforeEach(() => {
      failed = false;
    });

    it('calls backend; constructs and returns array of Todo', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        let response: Todo[];
        service.getTodos().subscribe(next => response = next, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush({
          foo1: { title: 'bar1', description: 'baz1' },
          foo2: { title: 'bar2', description: 'baz2' }
        });

        expect(response).toEqual([new Todo('foo1', 'bar1', 'baz1'), new Todo('foo2', 'bar2', 'baz2')]);
        expect(failed).toBe(false);
    })));

    it('fails if backend call fails', async(inject([TodosService, HttpTestingController],
      (service: TodosService, backend: HttpTestingController) => {
        service.getTodos().subscribe(next => {}, error => failed = true);

        backend.expectOne(requestMatcher)
        .flush(null, { status: 500, statusText: 'Error' });

        expect(failed).toBe(true);
    })));
  });
});
