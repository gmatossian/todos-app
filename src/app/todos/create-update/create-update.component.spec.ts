import { Todo } from './../todo.model';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CreateUpdateComponent } from './create-update.component';
import { TodosService } from './../todos.service';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { Router, ActivatedRoute, Params } from '@angular/router';


describe('CreateUpdateComponent', () => {
  let todosService: TodosService;
  let fixture: ComponentFixture<CreateUpdateComponent>;
  let component: CreateUpdateComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
        FormsModule
      ],
      providers: [
        TodosService
      ],
      declarations: [CreateUpdateComponent]
    });
    todosService = TestBed.get(TodosService);
    fixture = TestBed.createComponent(CreateUpdateComponent);
    component = fixture.componentInstance;
  });

  describe(`CreateUpdateComponent's attributes`, () => {
    it('defines and initializes attributes', () => {
      expect(component.form).toBeDefined();
      expect(component.id).not.toBeDefined();
      expect(component.todo).not.toBeDefined();
      expect(component.editMode).toBe(false);
    });
  });

  describe('ngOnInit', () => {
    it('sets id and edit mode when route params change', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      const todo = new Todo('foo', 'bar', 'baz');
      spyOn(todosService, 'getTodoById').and.returnValue(Observable.of(todo));
      const params = <Params>{
        id: 'qux'
      };
      (component as any).route.params.next(params);
      expect(component.id).toBe('qux');
      expect(component.editMode).toBe(true);
      expect(component.todo).toBe(todo);
    }));

    it(`doesn't fetch todo if there's no id`, fakeAsync(() => {
      fixture.detectChanges();
      tick();
      const todo = new Todo('foo', 'bar', 'baz');
      spyOn(todosService, 'getTodoById');
      const params = <Params>{
        id: undefined
      };
      (component as any).route.params.next(params);
      expect(component.id).toBe(undefined);
      expect(component.editMode).toBe(false);
      expect(todosService.getTodoById).not.toHaveBeenCalled();
    }));
  });
});
