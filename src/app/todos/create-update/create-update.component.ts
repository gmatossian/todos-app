import { Todo } from './../todo.model';
import { TodosService } from './../todos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create-update.component.html'
})
export class CreateUpdateComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  id: string;
  todo: Todo;
  editMode = false;

  constructor(private todosService: TodosService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params['id'];
        this.editMode = !!params['id'];

        if (this.editMode) {
          this.todosService.getTodoById(this.id)
          .subscribe(
            (todo: Todo) => {
              this.form.setValue({
              'title': todo.title,
              'description': todo.description
              });
              this.todo = todo;
            },
            (error) => console.log('Failed to load todo by ID: ' + JSON.stringify(error))
          );
        }
      }
    );
  }

  onCancel() {
    this.router.navigate(['']);
  }

  onSubmit(form: NgForm) {
    if (this.editMode) {
      this.todosService.updateTodo(this.id, form.value.title, form.value.description)
      .subscribe(
        (response) => this.todosService.onTodoAdded.next({todo: this.todo, added: false, edited: true, deleted: false}),
        (error) => console.log('Failed to update todo: ' + JSON.stringify(error))
      );
    } else {
      this.todosService.addTodo(form.value.title, form.value.description)
      .subscribe(
        (response: {name: string}) => {  // TODO: Maybe map response in the service and just return a string?
          const todo = new Todo(response.name, form.value.title, form.value.description);
          this.todosService.onTodoAdded.next({todo: this.todo, added: false, edited: true, deleted: false});
        },
        (error) => console.log('Failed to add todo: ' + JSON.stringify(error))
      );
    }
    this.router.navigate(['']);
  }

}
