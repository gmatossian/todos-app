import { AuthService } from './../../auth/auth.service';
import { TodosService } from './../todos.service';
import { Subscription } from 'rxjs/Subscription';
import { Todo } from './../todo.model';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-list-of-todos',
  templateUrl: './list-of-todos.component.html'
})
export class ListOfTodosComponent implements OnInit, OnDestroy {
  todos: Todo[] = [];
  filteredTodos: Todo[] = [];
  onTodoAddedSubscription: Subscription;
  keyword: string;

  constructor(private todosService: TodosService, private authService: AuthService) { }

  ngOnInit() {
    this.loadTodos();

    this.onTodoAddedSubscription = this.todosService.onTodoAdded.subscribe(
      (next: {todo: Todo, added: boolean, edited: boolean, deleted: boolean}) => {
        console.log('UPDATING LIST OF TODOS AFTER MODIFICATION: ' + JSON.stringify(next));
        this.loadTodos();
      }
    );
  }

  loadTodos() {
    this.todosService.getTodos()
    .subscribe(
      (todos: Todo[]) => this.setTodos(todos),
      (error) => console.log('Failed to get todos: ' + JSON.stringify(error))
    );
  }

  onDelete(todo: Todo) {
    this.todosService.deleteTodo(todo)
    .subscribe(
      (response) => this.todosService.onTodoAdded.next({todo: todo, added: false, edited: false, deleted: true}),
      (error) => console.log('Failed to delete todo: ' + JSON.stringify(error))
    );
  }

  ngOnDestroy() {
    this.onTodoAddedSubscription.unsubscribe();
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  onKeywordChange(keyword: string) {
    this.filterTodos();
  }

  filterTodos() {
    this.filteredTodos = this.todos.filter(
      (todo: Todo) => {
        if (this.keyword) {
          return (todo.title && todo.title.toLocaleLowerCase().includes(this.keyword.toLocaleLowerCase()))
            || (todo.description && todo.description.toLocaleLowerCase().includes(this.keyword.toLocaleLowerCase()));
        }
        return true;
      }
    );
  }

  setTodos(todos: Todo[]) {
    this.todos = todos;
    this.filterTodos();
  }

}
