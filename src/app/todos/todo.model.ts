export class Todo {
  public id: string;
  public title: string;
  public description: string;

  constructor(id: string, title: string, desc: string) {
    this.id = id;
    this.title = title;
    this.description = desc;
  }
}
