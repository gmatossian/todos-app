import { AuthService } from './../auth/auth.service';
import { Todo } from './todo.model';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class TodosService {
  todos: Todo[];
  onTodoAdded = new Subject<{todo: Todo, added: boolean, edited: boolean, deleted: boolean}>();  // TODO: Maybe create a model for it?

  constructor(private http: HttpClient) { }

  getTodos() {
    return this.http.get(`${environment.apiUrl}/todos/${environment.uidPlaceholder}.json`)
    .map(
      (body: Todo[]) => {
        const todos: Todo[] = [];
        if (body) {  // Body will be null if there're no todos
          Object.keys(body).forEach(
            (key: string) => todos.push(new Todo(key, body[key].title, body[key].description))
          );
        }
        return todos;
      }
    ).catch(
      (error: Response) => Observable.throw('Failed to load todos')
    );
  }

  addTodo(title: string, description: string) {
    return this.http.post(`${environment.apiUrl}/todos/${environment.uidPlaceholder}.json`, {
      title: title,
      description: description
    });
  }

  updateTodo(id: string, title: string, description: string) {
    return this.http.patch(`${environment.apiUrl}/todos/${environment.uidPlaceholder}/${id}.json`, {
      title: title,
      description: description
    });
  }

  deleteTodo(todo: Todo) {
    return this.http.delete(`${environment.apiUrl}/todos/${environment.uidPlaceholder}/${todo.id}.json`);
  }

  getTodoById(id: string) {
    return this.http.get(`${environment.apiUrl}/todos/${environment.uidPlaceholder}/${id}.json`)
    .map(
      (body: Todo) => {
        return new Todo(id, body.title, body.description);
      }
    ).catch(
      (error: Response) => Observable.throw('Failed to load todo')
    );
  }

}
