import { By } from '@angular/platform-browser';
import { TodosComponent } from './todos.component';
import { TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-list-of-todos',
  template: '<div mock-list-of-todos></div>'
})
class MockListOfTodosComponent {}

class MockAuthService {
  isAuthenticated() {
    return false;
  }
  isVerified() {
    return false;
  }
}

describe('TodosComponent', () => {
  let fixture: ComponentFixture<TodosComponent>;
  let component: TodosComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockListOfTodosComponent,
        TodosComponent
      ],
      providers: [
        {provide: AuthService, useClass: MockAuthService}
      ]
    });
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
  });

  it('includes the list of todos', () => {
    const el = fixture.debugElement.query(By.css('div[mock-list-of-todos]'));
    expect(el).toBeTruthy();
  });

  it('isAuthenticated returns true if service\'s isAuthenticated returns true', inject([AuthService], (authService: AuthService) => {
    spyOn(authService, 'isAuthenticated').and.returnValue(true);
    expect(component.isAuthenticated()).toBe(true);
  }));

  it('isAuthenticated returns false service\'s isAuthenticated returns false', inject([AuthService], (authService: AuthService) => {
    spyOn(authService, 'isAuthenticated').and.returnValue(false);
    expect(component.isAuthenticated()).toBe(false);
  }));

  it('isVerified returns true service\'s isVerified returns true', inject([AuthService], (authService: AuthService) => {
    spyOn(authService, 'isVerified').and.returnValue(true);
    expect(component.isVerified()).toBe(true);
  }));

  it('isVerified returns false service\'s isVerified returns false', inject([AuthService], (authService: AuthService) => {
    spyOn(authService, 'isVerified').and.returnValue(false);
    expect(component.isVerified()).toBe(false);
  }));
});
