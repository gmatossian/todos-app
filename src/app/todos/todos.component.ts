import { AuthService } from './../auth/auth.service';
import { Todo } from './todo.model';
import { TodosService } from './todos.service';
import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html'
})
export class TodosComponent {

  constructor(private authService: AuthService) { }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  isVerified() {
    return this.authService.isVerified();
  }

}
