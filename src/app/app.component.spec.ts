import { By } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import * as firebase from 'firebase';
import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { Component } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { MockComponent } from 'ng2-mock-component';
import { environment } from '../environments/environment';

@Component({ template: '<div dummy></div>' })
class DummyComponent {
}

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([
        {path: 'foo', component: DummyComponent}
      ])],
      declarations: [
        MockComponent({ selector: 'app-header', template: '<div app-header-mock></div>' }),
        AppComponent,
        DummyComponent
      ],
      providers: [{ provide: APP_BASE_HREF, useValue : '/' }]
    });
    router = TestBed.get(Router);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('initializes app', () => {
      spyOn(firebase, 'initializeApp');
      component.ngOnInit();
      expect(firebase.initializeApp).toHaveBeenCalledWith({
        apiKey: environment.apiKey,
        authDomain: environment.authDomain
      });
    });
  });

  describe('template', () => {
    it('has a header', () => {
      const header = fixture.debugElement.query(By.css('div[app-header-mock]'));
      expect(header).toBeTruthy();
    });

    it('has a router outlet', fakeAsync(() => {  // Navigation is an async
      expect(fixture.debugElement.query(By.css('div[dummy]'))).toBeFalsy();
      router.navigate(['foo']);
      tick();
      expect(fixture.debugElement.query(By.css('div[dummy]'))).toBeTruthy();
    }));
  });
});
