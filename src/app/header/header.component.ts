import { Router } from '@angular/router';
import { AuthService } from './../auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {

  constructor(private authService: AuthService, private router: Router) { }

  onLogout() {
    return this.authService.logout().then(() => {
      this.router.navigate(['unauthenticated']);
    });
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

}
