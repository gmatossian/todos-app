import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from './../auth/auth.service';
import { HeaderComponent } from './header.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { Router } from '@angular/router';


describe('UnauthenticatedComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let router: Router;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      declarations: [HeaderComponent],
      providers: [AuthService]
    });
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    authService = TestBed.get(AuthService);
  });

  describe('onLogout', () => {
    it('calls logout and navigates away if service call succeeds', fakeAsync(() => {
      spyOn(authService, 'logout').and.returnValue(Promise.resolve({}));
      spyOn(router, 'navigate');
      component.onLogout();
      tick();
      expect(router.navigate).toHaveBeenCalledWith(['unauthenticated']);
    }));

    it('calls logout and does not navigate away if service call fails', fakeAsync(() => {
      spyOn(authService, 'logout').and.returnValue(Promise.reject({}));
      spyOn(router, 'navigate');
      let failed = false;
      component.onLogout().catch(error => failed = true);
      tick();
      expect(failed).toBe(true);
      expect(router.navigate).not.toHaveBeenCalled();
    }));
  });

  describe('isAuthenticated', () => {
    it('returns true if the service returns true', () => {
      spyOn(authService, 'isAuthenticated').and.returnValue(true);
      expect(component.isAuthenticated()).toBe(true);
    });

    it('returns false if the service returns false', () => {
      spyOn(authService, 'isAuthenticated').and.returnValue(false);
      expect(component.isAuthenticated()).toBe(false);
    });
  });

  describe('template', () => {
    it('has a link to navigate to todos', () => {
      fixture.detectChanges();
      const anchor = fixture.debugElement.query(By.css('.navbar-header a')).nativeElement;
      expect(anchor.textContent.trim()).toBe('TODOs');
      expect(anchor.getAttribute('routerLink')).toBe('/');
    });
    it('has a link to navigate to home', () => {
      fixture.detectChanges();
      const anchor = fixture.debugElement.query(By.css('ul.navbar-nav li a')).nativeElement;
      expect(anchor.textContent.trim()).toBe('Home');
      expect(anchor.getAttribute('routerLink')).toBe('/');
    });

    it('has sing up / login buttons if user is not authenticated', () => {
      spyOn(component, 'isAuthenticated').and.returnValue(false);
      fixture.detectChanges();
      const buttons = fixture.debugElement.queryAll(By.css('ul.navbar-right li a'));
      expect(buttons.length).toBe(2);
      expect(buttons[0].nativeElement.textContent.trim()).toBe('Sign Up');
      expect(buttons[0].nativeElement.getAttribute('routerLink')).toBe('/signup');
      expect(buttons[1].nativeElement.textContent.trim()).toBe('Login');
      expect(buttons[1].nativeElement.getAttribute('routerLink')).toBe('/signin');
    });

    it('logout calls onLogout() when clicked', () => {
      spyOn(component, 'isAuthenticated').and.returnValue(true);
      spyOn(component, 'onLogout');
      fixture.detectChanges();
      const button = fixture.debugElement.query(By.css('ul.navbar-right li a')).nativeElement;
      expect(button.textContent.trim()).toBe('Logout');
      button.dispatchEvent(new Event('click'));
      expect(component.onLogout).toHaveBeenCalled();
    });
  });
});
