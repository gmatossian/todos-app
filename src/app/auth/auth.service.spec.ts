import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { AuthService } from './auth.service';
import * as firebase from 'firebase';

describe('AuthService', () => {
  let service: AuthService;
  let authMock;  // TODO: Provide type definition

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService]
    });
    service = TestBed.get(AuthService);
    authMock = {
      createUserWithEmailAndPassword: () => {},
      signInWithEmailAndPassword: () => {},
      currentUser: <firebase.User>{
        getToken: () => Promise.resolve('foo'),
        uid: 'bar',
        emailVerified: true,
        sendEmailVerification: () => {}
      },
      signOut: () => {}
    };
  });

  describe(`AuthService's attributes`, () => {
    it('defines and initializes attributes', () => {
      expect(service.token).toBe(null);
      expect(service.currentUser).toBe(null);
    });
  });

  describe('signup', () => {
    it('calls createUserWithEmailAndPassword passing along the data', () => {
      spyOn(firebase, 'auth').and.returnValue(authMock);
      const promise = Promise.resolve({});
      spyOn(authMock, 'createUserWithEmailAndPassword').and.returnValue(promise);
      const response = service.signup('foo', 'bar');
      expect(authMock.createUserWithEmailAndPassword).toHaveBeenCalledWith('foo', 'bar');
      expect(response).toBe(promise);
    });
  });

  describe('signin', () => {
    it('calls signInWithEmailAndPassword passing along the data', fakeAsync(() => {
      spyOn(firebase, 'auth').and.returnValue(authMock);
      const promise = Promise.resolve({});
      spyOn(authMock, 'signInWithEmailAndPassword').and.returnValue(promise);
      spyOn(service, 'getToken');
      const response = service.signin('foo', 'bar');
      tick();
      expect(authMock.signInWithEmailAndPassword).toHaveBeenCalledWith('foo', 'bar');
      expect(response).toBeTruthy();
      expect(service.currentUser).toBe(authMock.currentUser);
      expect(service.getToken).toHaveBeenCalled();
    }));
  });

  describe('getToken', () => {
    it('updates token and returns current value', fakeAsync(() => {
      spyOn(authMock.currentUser, 'getToken').and.callThrough();
      service.token = 'current value';
      service.currentUser = authMock.currentUser;
      expect(service.getToken()).toBe('current value');
      tick();
      expect(service.token).toBe('foo');
    }));
  });

  describe('getUid', () => {
    it(`returns current user's uid (if there's a current user)`, () => {
      service.currentUser = authMock.currentUser;
      expect(service.getUid()).toBe('bar');
    });

    it(`returns undefined if there's no current user`, () => {
      expect(service.getUid()).toBe(undefined);
    });
  });

  describe('isAuthenticated', () => {
    it(`returns true if there's a current user`, () => {
      service.currentUser = authMock.currentUser;
      expect(service.isAuthenticated()).toBe(true);
    });

    it(`returns undefined (if there's no current user)`, () => {
      expect(service.isAuthenticated()).toBe(false);
    });
  });

  describe('isVerified', () => {
    it(`returns true if there's a current (verified) user`, () => {
      service.currentUser = authMock.currentUser;
      expect(service.isVerified()).toBe(true);
    });

    it(`returns false if there's a current (unverified) user`, () => {
      service.currentUser = authMock.currentUser;
      authMock.currentUser.emailVerified = false;
      expect(service.isVerified()).toBe(false);
    });

    it(`returns undefined if there's no current user`, () => {
      expect(service.isVerified()).toBe(false);
    });
  });

  describe('sendVerificationEmail', () => {
    it(`returns true if there's a current (verified) user`, () => {
      const promise = Promise.resolve({});
      spyOn(authMock.currentUser, 'sendEmailVerification').and.returnValue(promise);
      service.currentUser = authMock.currentUser;
      expect(service.sendVerificationEmail()).toBe(promise);
    });
  });

  describe('logout', () => {
    it(`calls signOut and sets token and currentUser to null if call succeeds`, fakeAsync(() => {
      spyOn(firebase, 'auth').and.returnValue(authMock);
      const promise = Promise.resolve({});
      spyOn(authMock, 'signOut').and.returnValue(promise);
      expect(service.logout()).toBeTruthy();  // There's no way to check for obj reference
      tick();
      expect(service.token).toBe(null);
      expect(service.currentUser).toBe(null);
    }));

    it(`calls signOut and doesn't sets token nor currentUser if call fails`, fakeAsync(() => {
      spyOn(firebase, 'auth').and.returnValue(authMock);
      const promise = Promise.reject({});
      spyOn(authMock, 'signOut').and.returnValue(promise);
      service.token = 'foo';
      service.currentUser = authMock.currentUser;
      let failed = false;
      service.logout().catch(error => failed = true);
      tick();
      expect(failed).toBe(true);
      expect(service.token).toBe('foo');
      expect(service.currentUser).toBe(authMock.currentUser);
    }));
  });
});
