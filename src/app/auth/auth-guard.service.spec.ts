import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let service: AuthService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        AuthService,
        AuthGuard
      ]
    });
    guard = TestBed.get(AuthGuard);
    service = TestBed.get(AuthService);
    router = TestBed.get(Router);
  });

  describe('canActivate', () => {
    it('returns false an navigates away if user is not authenticated', fakeAsync(() => {
      spyOn(service, 'isAuthenticated').and.returnValue(false);
      spyOn(router, 'navigate');
      expect(guard.canActivate(null, null)).toBe(false);
      tick();
      expect(router.navigate).toHaveBeenCalledWith(['unauthenticated']);
    }));

    it('returns false an navigates away if user authenticated but not verified', fakeAsync(() => {
      spyOn(service, 'isAuthenticated').and.returnValue(true);
      spyOn(service, 'isVerified').and.returnValue(false);
      spyOn(router, 'navigate');
      expect(guard.canActivate(null, null)).toBe(false);
      tick();
      expect(router.navigate).toHaveBeenCalledWith(['unverified']);
    }));

    it('returns true if user authenticated and verified', fakeAsync(() => {
      spyOn(service, 'isAuthenticated').and.returnValue(true);
      spyOn(service, 'isVerified').and.returnValue(true);
      spyOn(router, 'navigate');
      expect(guard.canActivate(null, null)).toBe(true);
      tick();
      expect(router.navigate).not.toHaveBeenCalled();
    }));
  });
});
