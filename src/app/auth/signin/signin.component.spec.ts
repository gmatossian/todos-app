import { dispatchEvent } from '@angular/platform-browser/testing/src/browser_util';
import { Observable } from 'rxjs/Observable';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, NgForm, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { AuthService } from './../auth.service';
import { SigninComponent } from './signin.component';
import { Router, RouterModule } from '@angular/router';
import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';


describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;
  let authService: AuthService;
  let formEl: DebugElement;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SigninComponent],
      imports: [
        RouterTestingModule.withRoutes([]),
        ReactiveFormsModule
      ],
      providers: [AuthService]
    });
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
    formEl = fixture.debugElement.query(By.css('form'));
  });

  describe(`SigninComponent's attributes`, () => {
    it('defines and initializes attributes', () => {
      expect(component.failure).toBe(false);
    });
  });

  describe('onSignIn', () => {
    it('makes a service call passing the form data and navigates away', fakeAsync(() => {
      spyOn(authService, 'signin').and.returnValue(Promise.resolve({}));
      spyOn(router, 'navigate');
      component.ngOnInit();
      component.form.setValue({ email: 'foo', password: 'bar' });
      component.onSignIn();
      tick();
      expect(authService.signin).toHaveBeenCalledWith('foo', 'bar');
      expect(router.navigate).toHaveBeenCalledWith(['']);
      expect(component.failure).toBe(false);
    }));

    it('sets failure to true if service call fails', fakeAsync(() => {
      spyOn(authService, 'signin').and.returnValue(Promise.reject({}));
      spyOn(router, 'navigate');
      component.ngOnInit();
      component.form.setValue({ email: 'foo', password: 'bar' });
      component.onSignIn();
      tick();
      expect(authService.signin).toHaveBeenCalled();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(component.failure).toBe(true);
    }));
  });

  describe('email', () => {
    it('return forms email control', () => {
      component.ngOnInit();
      expect(component.email).toEqual(component.form.get('email'));
    });

    it('return forms password control', () => {
      component.ngOnInit();
      expect(component.password).toEqual(component.form.get('password'));
    });
  });

  describe('template', () => {
    it('displays error message if failure is true', () => {
      expect(fixture.debugElement.query(By.css('div.alert.alert-danger'))).toBeFalsy();
      component.failure = true;
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('div.alert.alert-danger'));
      expect(el).toBeTruthy();
      expect(el.nativeElement.textContent.trim()).toBe('Invalid username or password');
    });

    it('has a form', () => {
      expect(formEl).toBeTruthy();
    });

    it('has a submit button, enabled if form is valid', () => {
      fixture.detectChanges();
      expect(component.form.valid).toBe(false);
      expect(fixture.debugElement.query(By.css('form button[type=submit][disabled]'))).toBeTruthy();
      component.email.setValue('test@test.com');
      component.password.setValue('password');
      expect(component.form.valid).toBe(true);
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('form button[type=submit][disabled]'))).toBeFalsy();
    });

    it('does not show validation errors if inputs are empty but not touched', () => {
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#emailErrors span'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('#pwdErrors span'))).toBeFalsy();
    });

    it('shows required errors if inputs are empty (and touched)', () => {
      fixture.detectChanges();
      component.email.markAsTouched();
      component.password.markAsTouched();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#emailErrors span')).nativeElement.textContent.trim())
        .toBe('Email is required');
      expect(fixture.debugElement.query(By.css('#pwdErrors span')).nativeElement.textContent.trim())
        .toBe('Password is required');
      expect(component.email.errors.required).toBe(true);
      expect(component.email.errors.email).toBe(true);
    });

    it('shows invalid email error if email is not valid (and input is touched)', () => {
      fixture.detectChanges();
      component.email.markAsTouched();
      component.email.setValue('invalid email');
      fixture.detectChanges();
      expect(component.email.errors.required).not.toBeDefined();
      expect(component.email.errors.email).toBe(true);
      expect(fixture.debugElement.query(By.css('#emailErrors span')).nativeElement.textContent.trim())
        .toBe('Email is invalid');
    });

    it('does not show any errors if fields are valid', () => {
      fixture.detectChanges();
      component.email.setValue('test@test.com');
      component.password.setValue('password');
      component.email.markAsTouched();
      component.password.markAsTouched();
      fixture.detectChanges();
      expect(component.email.errors).toBeFalsy();
      expect(component.password.errors).toBeFalsy();
      expect(component.form.valid).toBe(true);
      expect(fixture.debugElement.query(By.css('#emailErrors span'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('#pwdErrors span'))).toBeFalsy();
    });
  });
});
