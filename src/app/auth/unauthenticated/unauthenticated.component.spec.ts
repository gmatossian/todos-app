import { UnauthenticatedComponent } from './unauthenticated.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { TestBed, ComponentFixture } from '@angular/core/testing';


describe('UnauthenticatedComponent', () => {
  let component: UnauthenticatedComponent;
  let fixture: ComponentFixture<UnauthenticatedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UnauthenticatedComponent]
    });
    fixture = TestBed.createComponent(UnauthenticatedComponent);
    component = fixture.componentInstance;
  });

  describe('template', () => {
    it('has a heading', () => {
      fixture.detectChanges();
      const heading = fixture.debugElement.query(By.css('div.well h4')).nativeElement;
      expect(heading.textContent.trim()).toBe('Welcome!');
    });

    it('has a paragraph', () => {
      fixture.detectChanges();
      const heading = fixture.debugElement.query(By.css('div.well p')).nativeElement;
      expect(heading.textContent.trim()).toBe('Please sign in to start.');
    });
  });
});
