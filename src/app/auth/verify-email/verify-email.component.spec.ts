import { AuthService } from './../auth.service';
import { VerifyEmailComponent } from './verify-email.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';


describe('UnauthenticatedComponent', () => {
  let component: VerifyEmailComponent;
  let fixture: ComponentFixture<VerifyEmailComponent>;
  let authService: AuthService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [VerifyEmailComponent],
      providers: [AuthService]
    });
    fixture = TestBed.createComponent(VerifyEmailComponent);
    component = fixture.componentInstance;
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
  });

  describe(`VerifyEmailComponent's attributes`, () => {
    it('defines and initializes attributes', () => {
      expect(component.emailSent).toBe(false);
      expect(component.failure).toBe(false);
    });
  });

  describe('sendVerificationEmail', () => {
    it('sets emailSent to true, logs out, and navigates away', fakeAsync(() => {
      spyOn(authService, 'sendVerificationEmail').and.returnValue(Promise.resolve({}));
      spyOn(authService, 'logout').and.returnValue(Promise.resolve({}));
      spyOn(router, 'navigate');
      component.sendVerificationEmail();
      tick();
      expect(component.failure).toBe(false);
      expect(component.emailSent).toBe(true);
      expect(authService.logout).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['unauthenticated']);
    }));

    it('sets failure to true if service call fails', fakeAsync(() => {
      spyOn(authService, 'sendVerificationEmail').and.returnValue(Promise.reject({}));
      spyOn(authService, 'logout').and.returnValue(Promise.resolve({}));
      spyOn(router, 'navigate');
      component.sendVerificationEmail();
      tick();
      expect(component.emailSent).toBe(false);
      expect(component.failure).toBe(true);
      expect(authService.logout).not.toHaveBeenCalled();
      expect(router.navigate).not.toHaveBeenCalled();
    }));
  });

  describe('template', () => {
    it('displays error message if something went wrong', () => {
      component.failure = true;
      fixture.detectChanges();
      const heading = fixture.debugElement.query(By.css('div.alert.alert-danger')).nativeElement;
      expect(heading.textContent.trim()).toBe('Something went wrong, please try again later.');
    });

    it('does not display error message if nothing went wrong', () => {
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('div.alert.alert-danger'))).toBeFalsy();
    });

    it('displays email sent message if email was sent', () => {
      component.emailSent = true;
      fixture.detectChanges();
      const heading = fixture.debugElement.query(By.css('#emailSent h4')).nativeElement;
      expect(heading.textContent.trim()).toBe('Email sent!');
      const paragraph = fixture.debugElement.query(By.css('#emailSent p')).nativeElement;
      expect(paragraph.textContent.trim()).toBe('Please check your inbox for the email and follow the instructions in it. '
        + 'Log back in once the email is verified.');
    });

    it('displays almost done message if email was not sent and there is a send email button', () => {
      fixture.detectChanges();
      const heading = fixture.debugElement.query(By.css('#emailNotSent h4')).nativeElement;
      expect(heading.textContent.trim()).toBe('Almost done!');
      const paragraph = fixture.debugElement.query(By.css('#emailNotSent p')).nativeElement;
      expect(paragraph.textContent.trim()).toBe('Please verify your email to complete the registration process');
      const sendButton = fixture.debugElement.query(By.css('button')).nativeElement;
      expect(sendButton.textContent.trim()).toBe('Send verification email');
    });
  });
});
