import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html'
})
export class VerifyEmailComponent {
  emailSent = false;
  failure = false;

  constructor(private authService: AuthService, private router: Router) { }

  sendVerificationEmail() {
    this.authService.sendVerificationEmail()
    .then(
      (response: any) => {
        this.emailSent = true;
        this.authService.logout().then(() => {
          this.router.navigate(['unauthenticated']);
        });
      }
    )
    .catch(
      error => this.failure = true
    );
  }

}
