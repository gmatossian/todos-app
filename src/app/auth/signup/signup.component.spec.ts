import { SignupComponent } from './signup.component';
import { dispatchEvent } from '@angular/platform-browser/testing/src/browser_util';
import { Observable } from 'rxjs/Observable';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, NgForm, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { AuthService } from './../auth.service';
import { Router, RouterModule } from '@angular/router';
import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';


describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let authService: AuthService;
  let formEl: DebugElement;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SignupComponent],
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule
      ],
      providers: [AuthService]
    });
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
    formEl = fixture.debugElement.query(By.css('form'));
  });

  describe(`SignupComponent's attributes`, () => {
    it('defines and initializes attributes', () => {
      expect(component.failure).toBe(false);
    });
  });

  describe('onSignUp', () => {
    it('makes a service call passing the form data and navigates away', fakeAsync(() => {
      spyOn(authService, 'signup').and.returnValue(Promise.resolve({}));
      spyOn(router, 'navigate');
      const form = <NgForm>{ value: { email: 'foo', password: 'bar' } };
      component.onSignUp(form);
      tick();
      expect(authService.signup).toHaveBeenCalledWith('foo', 'bar');
      expect(router.navigate).toHaveBeenCalledWith(['']);
      expect(component.failure).toBe(false);
    }));

    it('sets failure to true if service call fails', fakeAsync(() => {
      spyOn(authService, 'signup').and.returnValue(Promise.reject({}));
      spyOn(router, 'navigate');
      const form = <NgForm>{ value: { email: 'foo', password: 'bar' } };
      component.onSignUp(form);
      tick();
      expect(authService.signup).toHaveBeenCalled();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(component.failure).toBe(true);
    }));
  });

  describe('template', () => {
    it('displays error message if failure is true', () => {
      expect(fixture.debugElement.query(By.css('div.alert.alert-danger'))).toBeFalsy();
      component.failure = true;
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('div.alert.alert-danger'));
      expect(el).toBeTruthy();
      expect(el.nativeElement.textContent.trim()).toBe('Something went wrong, please try again later.');
    });

    it('has a form', () => {
      expect(formEl).toBeTruthy();
    });

    it('has a submit button, enabled if form is valid', fakeAsync(() => {
      fixture.detectChanges();
      tick();  // Required in order for form controls to be registered properly
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('form button[type=submit][disabled]'))).toBeTruthy();
      const emailEl = fixture.debugElement.query(By.css('#email')).nativeElement;
      emailEl.value = 'test@test.com';
      emailEl.dispatchEvent(new Event('input'));
      const passEl = fixture.debugElement.query(By.css('#password')).nativeElement;
      passEl.value = 'password';
      passEl.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('form button[type=submit][disabled]'))).toBeFalsy();
    }));

    it('does not show validation errors if inputs are empty but not touched', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#emailErrors span'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('#pwdErrors span'))).toBeFalsy();
    }));

    it('shows required errors if inputs are empty (and touched)', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
      const emailEl = fixture.debugElement.query(By.css('#email')).nativeElement;
      emailEl.dispatchEvent(new Event('input'));
      emailEl.dispatchEvent(new Event('blur'));
      const passEl = fixture.debugElement.query(By.css('#password')).nativeElement;
      passEl.dispatchEvent(new Event('input'));
      passEl.dispatchEvent(new Event('blur'));
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#emailErrors span')).nativeElement.textContent.trim())
        .toBe('Email is required');
      expect(fixture.debugElement.query(By.css('#pwdErrors span')).nativeElement.textContent.trim())
        .toBe('Password is required');
    }));

    it('shows invalid email error if email is not valid (and input is touched)', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
      const emailEl = fixture.debugElement.query(By.css('#email')).nativeElement;
      emailEl.value = 'invalid email';
      emailEl.dispatchEvent(new Event('input'));
      emailEl.dispatchEvent(new Event('blur'));
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#emailErrors span')).nativeElement.textContent.trim())
        .toBe('Email is invalid');
    }));

    it('shows invalid password error if password length < 6 (and input is touched)', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
      const passEl = fixture.debugElement.query(By.css('#password')).nativeElement;
      passEl.value = 'abcde';
      passEl.dispatchEvent(new Event('input'));
      passEl.dispatchEvent(new Event('blur'));
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#pwdErrors span')).nativeElement.textContent.trim())
        .toBe('Password must be 6 characters minimum');
    }));

    it('does not show any errors if fields are valid', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
      const emailEl = fixture.debugElement.query(By.css('#email')).nativeElement;
      emailEl.value = 'test@test.com';
      emailEl.dispatchEvent(new Event('input'));
      emailEl.dispatchEvent(new Event('blur'));
      const passEl = fixture.debugElement.query(By.css('#password')).nativeElement;
      passEl.value = 'password';
      passEl.dispatchEvent(new Event('input'));
      passEl.dispatchEvent(new Event('blur'));
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#emailErrors span'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('#pwdErrors span'))).toBeFalsy();
    }));
  });
});
