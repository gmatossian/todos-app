import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent {
  failure = false;

  constructor(private authService: AuthService, private router: Router) { }

  onSignUp(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authService.signup(email, password)
    .then(
      (response: any) => this.router.navigate([''])
    )
    .catch(
      (error: any) => this.failure = true
    );
  }

}
