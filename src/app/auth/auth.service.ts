import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { User } from 'firebase';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class AuthService {
  token: string = null;
  currentUser: User = null;  // Maybe use firebase.auth().currentUser instead?

  constructor() { }

  signup(email: string, password: string) {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  signin(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password)
    .then(
      response => {
        this.currentUser = firebase.auth().currentUser;
        this.getToken();
      }
    );
  }

  getToken() {
    this.currentUser.getToken()
    .then((token: string) => this.token = token);
    return this.token;
  }

  getUid() {
    return this.currentUser ? this.currentUser.uid : undefined;
  }

  isAuthenticated() {
    return this.currentUser !== null;
  }

  isVerified() {
    return !!(this.currentUser ? this.currentUser.emailVerified : undefined);
  }

  sendVerificationEmail() {
    return this.currentUser.sendEmailVerification();
  }

  logout() {
    return firebase.auth().signOut().then(() => {
      this.token = null;
      this.currentUser = null;
    });
  }

}
