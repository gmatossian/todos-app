import { AuthService } from './auth.service';
import { HttpRequest, HttpEvent } from '@angular/common/http';
import { HttpInterceptor, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedReq = req.clone({
      params: req.params.set(environment.authTokenParamName, this.authService.getToken()),
      url: req.url.replace(environment.uidPlaceholderRegex, this.authService.getUid())
    });
    return next.handle(clonedReq);
  }

}
