import { Observable } from 'rxjs/Observable';
import { HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';
import { AuthService } from './auth.service';
import { TestBed } from '@angular/core/testing';

describe('AuthInterceptor', () => {
  let interceptor: AuthInterceptor;
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        AuthInterceptor
      ]
    });
    interceptor = TestBed.get(AuthInterceptor);
    service = TestBed.get(AuthService);
  });

  describe('intercept', () => {
    it('adds uid to the URL and adds the token as a parameter', () => {
      const request = new HttpRequest('GET', 'some/backend/$UID$');
      const event = <HttpEvent<any>>{};
      const handler = <HttpHandler>{
        handle: (req: HttpRequest<any>) => Observable.of(event)
      };
      spyOn(service, 'getToken').and.returnValue('TOKEN');
      spyOn(service, 'getUid').and.returnValue('UID');
      spyOn(handler, 'handle');
      interceptor.intercept(request, handler);
      expect(handler.handle).toHaveBeenCalledWith(jasmine.objectContaining({
        urlWithParams: 'some/backend/UID?auth=TOKEN'
      }));
    });
  });
});
